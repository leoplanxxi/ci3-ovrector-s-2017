

<?php 

Class Seguimiento_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function asignarMensaje($idMensaje, $idPrioridad, $idDependencia, $anonimo) {
        $fecha = date("Y-m-d");
        $idusuario = $this->session->userdata("logged_in")["idusuario"]; //Revisar esto
 
        $data = array(
            'id_mensaje' => $idMensaje,
            'id_prioridad' => $idPrioridad,
            'id_estado' => 3,
            'id_dependencia' => $idDependencia,
            'fecha_asignacion' => $fecha,
            'Usuario_Asigna_ID' => $idusuario,
            'Anonimo' => $anonimo
        );

        $this->db->insert("seguimiento", $data);
        return $this->db->insert_id();
    }

    public function liberarMensajecEmail($idUsr, $idSeguimiento, $respuesta) {
        $query_01 = $this->db->query("UPDATE `contacto`.`seguimiento` SET `respuesta_mensaje` = ?, `fecha_respuesta` = CURDATE(), R = 1, `Fecha_respuesta_supervisor` = CURDATE(),  id_estado = 2 WHERE `seguimiento`.`id_seguimiento` = ?", array($respuesta, $idSeguimiento));

        $query = $this->db->query("UPDATE `contacto`.`seguimiento` SET `fecha_liberado` = CURDATE(), `Usuario_libera_ID` = ?, `Usuario_Responde_ID` = ?, id_estado = 5 WHERE `seguimiento`.`id_seguimiento` = ?", array($idUsr, $idUsr, $idSeguimiento));      
    }

    public function liberarMensajesEmail($idUsr, $idSeguimiento) {
        $query = $this->db->query("UPDATE `contacto`.`seguimiento` SET `fecha_liberado` = CURDATE(), `Usuario_libera_ID` = ?, id_estado = 4 WHERE `seguimiento`.`id_seguimiento` = ?", array($idUsr, $idSeguimiento));
    }
	
	public function desliberarMensaje($idSeguimiento) {
		$query = $this->db->query("UPDATE `contacto`.`seguimiento` SET `id_estado`=2 WHERE `id_seguimiento`=?", array($idSeguimiento));
		$query = $this->db->query("UPDATE `contacto`.`seguimiento` SET `respuesta_mensaje`='' WHERE `id_seguimiento`=?", array($idSeguimiento));
	}
	
	public function purgarMensaje($idSeguimiento) {
		$query = $this->db->query("UPDATE `contacto`.`seguimiento` SET `purgado`=1 WHERE  `id_seguimiento`=?", array($idSeguimiento));
	}
	
	public function despurgarMensaje($idSeguimiento) {
		$query = $this->db->query("UPDATE `contacto`.`seguimiento` SET `purgado`=0 WHERE  `id_seguimiento`=?", array($idSeguimiento));
	}
}