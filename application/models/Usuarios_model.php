<?php
  Class Usuarios_model extends CI_Model {
    public function __construct() {
      parent::__construct();
    }

    public function getUsuarios() {
      $resultado = $this->db->get("tb_usuarios");
      return $resultado->result_array();
    }

    public function getUsuario($idUsr) {
      $this->db->where("id_usuario", $idUsr);
      return $this->db->get("tb_usuarios")->result_array()[0];
    }

    public function getUsuariobyName($name) {
      $this->db->where("usuario", $name);
      return $this->db->get("tb_usuarios")->result_array()[0];
    }

    public function checkLogin($nombre, $pass) {
       $passwd_bcrypt = $this->db->query("SELECT password FROM tb_usuarios WHERE usuario = ?", array($nombre))->result_array()[0]["password"];

       $this->db->where("usuario", $nombre);
       $resultado = $this->db->get("tb_usuarios")->result_array();

       if (count($resultado) > 0) {
         if (password_verify($pass, $passwd_bcrypt)) {
           return true;
         }
         else {
           return false;
         }
       }
       else {
         return false;
       }
    }
  }
 ?>
