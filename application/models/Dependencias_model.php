<?php
  Class Dependencias_model extends CI_Model {
    public function __construct() {
      parent::__construct();
    }

    public function getAllDependencias() {
      $resultado = $this->db->get("dependencias");
      return $resultado->result_array();
    }

  }
 ?>
