<?php

Class Moderacion_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getAllModeracion() {
        return $this->db->get("moderacion")->result_array();
    } 

    public function getAllPrioridades() {
        return $this->db->get("prioridad")->result_array();
    }

}