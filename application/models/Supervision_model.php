<?php
  Class Supervision_model extends CI_Model {
    public function __construct() {
      parent::__construct();
    }

    public function getAllMensajes() {
      return $this->db->get("supervision")->result_array();
    }

    public function getAllMensajesDependencia($idDependencia) {
      $this->db->where("id_dependencia", $idDependencia);
      return $this->db->get("supervision")->result_array();
      
    }

    public function getMensaje($idMsg) {
      $this->db->where("id_mensaje", $idMsg);
      return $this->db->get("supervision")->result_array();
    }

    public function getMensajeEmailCliente($idSup) {
        $this->db->select("email_cliente");
        $this->db->where("id_seguimiento", $idSup);
        return $this->db->get("supervision")->result_array()[0]["email_cliente"];
    }
	
	public function getHistorialLibera() {
		return $this->db->get("historial_libera")->result_array();
	}
	
	public function getHistorialLibera_pag($limit, $start) {
		 $this->db->limit($limit, $start);
        $query = $this->db->get("historial_libera");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
	}
	
	public function getHistorialPurga() {
		return $this->db->get("historial_purga")->result_array();
	}

  }
 ?>
