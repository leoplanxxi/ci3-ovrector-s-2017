<?php

Class Exportar_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getAllExportar() {
        return $this->db->get("exportar");
    }

    public function getAllExportarSLiberar() {
        return $this->db->get("exportar_sliberar");
    }

    public function getAllExportarFechas() {
        $inicial = $this->secclean->limpiar($this->input->post("inicial"));
        $inicial = date("Y-m-d", strtotime($inicial));
        $final = $this->secclean->limpiar($this->input->post("final"));
        $final = date("Y-m-d", strtotime($final));
        $field = "Fecha de Asignación";
        $query = $this->db->query("SELECT * FROM exportar WHERE `" . $field . "` BETWEEN ? and ?", array($inicial, $final));
        
        return $query;
    }
}