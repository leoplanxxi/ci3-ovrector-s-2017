<?php

Class Historial_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function historialAsigna() {
        $query = $this->db->get("historial_asigna");
        return $query->result_array();
    }
	
	public function getHistorialAsigna_pag($limit, $start) {
		 $this->db->limit($limit, $start);
        $query = $this->db->get("historial_asigna");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
	}

    public function historialLibera() {
        $query = $this->db->get("historial_libera");
        return $query->result_array();
    }

    public function historialPurga() {
        $query = $this->db->get("historial_purga");
        return $query->result_array();
    }
}
?>