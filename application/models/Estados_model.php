<?php
  Class Estados_model extends CI_Model {
    public function __construct() {
      parent::__construct();
    }

    public function getEstados() {
      return $this->db->get("estados")->result_array();
    }
  }
 ?>
