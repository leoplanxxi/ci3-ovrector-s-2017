<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.18/jquery.touchSwipe.js" integrity="sha256-KKv6+21Uvs0xGccvranH3/RaUvz7N24khCbpW6leF5o=" crossorigin="anonymous"></script>
<script>
 function abrirCerrariFrame(idSeguimiento) {
    $("#respuesta-" + idSeguimiento).stop().slideToggle();
    return false;
 }
 
</script>
<script>
 $(document).ready(function() {
        $.post("<?php echo base_url("index.php/admin/exportarCSV_fecha"); ?>", function(item) {
            $("#modalExporta").html(item);
    });

    $('div[id^="swipe_"]').swipe({
  swipeRight:function(event, direction, distance, duration, fingerCount) {
    idseguimiento = $(this)[0].id;
    idseguimiento = idseguimiento.replace("swipe_","");
    console.log(idseguimiento);
    window.location.href = "<?php echo base_url("index.php/admin/liberarsMensaje/"); ?>" + idseguimiento;

   // alert("You swiped " + direction );
    
  }
});
 });

 $(window).load(function() {

	$("#agregar-propuesta").click(function () {
		$("#propuesta-form").stop().slideToggle();
		return false;
	});

 });
 

</script>

<body class="supervisor-page">
    <div class="container-fluid">
        <?php echo loadView('common/menu', NULL); ?>
        <br>
           </div>
    <div class="container">
        <h2 class="text-center">Módulo de Supervisión</h2>
        <div class="col-12">
            <a href="<?php echo base_url("index.php/admin/historial_liberados"); ?>"><button class="btn btn-outline-primary">
                <i class="fa fa-history" aria-hidden="true"></i> Historial
            </button></a>
            <button class="btn btn-outline-primary" data-toggle="modal" data-target="#modalExportar">
                <i class="fa fa-floppy-o" aria-hidden="true"></i> Exportar a CSV
            </button>
        </div>
        <?php echo form_open(); ?>
        <div class="col-12">            
            <br><br> <?php if (count($mensajes) == 0): ?>
				<p class="text-center">No hay mensajes en este buzón</p>
			<?php endif; ?>
            <?php $c = 1; ?>
            <?php foreach ($mensajes as $mensaje): ?>
<div class="card" id="swipe_<?php echo $mensaje["id_seguimiento"]; ?>">

  <div class="card-block">
    <p><b>De: </b> 
    <?php  if ($mensaje["Anonimo"] != "1") {
            echo $mensaje["nombre_cliente"]; }
        else {
            echo "Anónimo"; }
            ?> (<?php 
            if ($mensaje["Anonimo"] != "1") {
                echo $mensaje["email_cliente"]; }
            else {
                echo "Anónimo"; 
            }?><?php 
                
            if ($mensaje["telefono"] != 0 && $mensaje["Anonimo"] != "1"): ?> - <?php echo $mensaje["telefono"]; ?><?php endif;?>)</p>
    <p><b>Asunto: </b><?php echo $mensaje["asunto"]; ?></p>
    <p><?php echo $mensaje["mensaje"]; ?></p>
	<?php 
	$appbuap = $this->db->query("SELECT appbuap FROM mensaje WHERE id_mensaje = ?", array($mensaje["id_mensaje"]))->result_array()[0]["appbuap"];
	?>
	<?php if ($appbuap == "1"): ?>
	<i class="text-right" style="display: block; padding-bottom: 15px;">Enviado desde AppBUAP</i>
	<?php endif; ?>
    <div class="container-fluid container-botones-liberar"> 
        <div class="row">
            <div class="col-lg-12">
                <a title="Liberar con respuesta" class="btn btn-success offset-lg-2 col-lg-4 col-md-12" id="liberar-c-respuesta-<?php echo $mensaje["id_seguimiento"]; ?>" onclick="abrirCerrariFrame('<?php echo $mensaje["id_seguimiento"]; ?>')" style="color: white; cursor: default;"><i class="fa fa-envelope" aria-hidden="true"></i> Liberar con respuesta </a>
                <a href="<?php echo base_url("index.php/admin/liberarsMensaje/" . $mensaje["id_seguimiento"]); ?>" title="Liberar sin respuesta" class="btn btn-warning col-lg-4 col-md-12" id="liberar-s-respuesta-<?php echo $mensaje["id_seguimiento"]; ?>"><i class="fa fa-times" aria-hidden="true"></i> Liberar sin respuesta </a>
            </div>
                    

        </div>
      </div>
    <div class="container-fluid container-editor">
      <div class="row">
          <iframe src="<?php echo base_url("index.php/admin/responder_iframe/" . $mensaje["id_seguimiento"]); ?>" class="col-12" scrolling=no id="respuesta-<?php echo $mensaje["id_seguimiento"]; ?>" style="display: none;"></iframe>
      </div>
  </div>
  </div>

</div>
<br>
            <?php endforeach; ?>
        </div>
    </div>
    <?php echo form_close(); ?>
    </div>
    <div id="modalExporta">

    </div>
</body>