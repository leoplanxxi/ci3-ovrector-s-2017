<?php echo form_open(); ?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="text-center">Exportar a CSV</h4>

            <p>Seleccione el rango de fechas de los mensajes que desea exportar: </p>
            <p>Fecha de inicio: <br>
            <input type="date" name="inicial"></p>
            <p>Fecha de fin: <br>
            <input type="date" name="final"></p>
        </div>
    </div>
</div>