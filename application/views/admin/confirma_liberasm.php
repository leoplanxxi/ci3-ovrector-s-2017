<script type="text/javascript">
    $(window).on('load',function(){
        $('#myModal').modal('show');
    });
</script>
<?php echo form_open(); ?>
<div class="modal fade" id="myModal" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirmar liberación sin mensaje</h5>
        <input type="hidden" name="idseg" value="<?php echo $idseg; ?>" />
      </div>
      <div class="modal-body">
        <p>Confirme que desea liberar <b>sin mensaje</b> el mensaje seleccionado.</p>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Liberar sin mensaje</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="javascript:history.back(1)">Cancelar</button>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>