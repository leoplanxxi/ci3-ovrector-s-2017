
<script>
 function abrirCerrariFrame(idMensaje) {
    $("#respuesta-" + idMensaje).stop().slideToggle();
    return false;
 }
 
</script>
<script>
 $(document).ready(function() {
        $.post("<?php echo base_url("index.php/admin/exportarCSV_fecha"); ?>", function(item) {
            $("#modalExporta").html(item);
    });
 });

 $(window).load(function() {

	$("#agregar-propuesta").click(function () {
		$("#propuesta-form").stop().slideToggle();
		return false;
	});

 });
 

</script>

<body class="supervisor-page">
    <div class="container-fluid">
        <?php echo loadView('common/menu', NULL); ?>
        <br>
           </div>
    <div class="container">
        <h2 class="text-center">Módulo de Moderación</h2>
        <div class="col-12">
            <a href="<?php echo base_url("index.php/admin/historial_asignados"); ?>"<button class="btn btn-outline-primary">
                <i class="fa fa-history" aria-hidden="true"></i> Historial
            </button></a>

        </div>
        <?php echo form_open(); ?>
        <div class="col-12">            
            <br><br>
			<?php if (count($mensajes) == 0): ?>
				<p class="text-center">No hay mensajes en este buzón</p>
			<?php endif; ?>
            <?php foreach ($mensajes as $mensaje): ?>
<div class="card">

  <div class="card-block">
    <p><b>De: </b> <?php echo $mensaje["nombre_cliente"]; ?> (<?php echo $mensaje["email_cliente"]; ?><?php if ($mensaje["telefono"] != 0): ?> - <?php echo $mensaje["telefono"]; ?><?php endif;?>)</p>
    <p><b>Asunto: </b><?php echo $mensaje["asunto"]; ?></p>
    <p><?php echo $mensaje["mensaje"]; ?></p>
	<?php 
	$appbuap = $this->db->query("SELECT appbuap FROM mensaje WHERE id_mensaje = ?", array($mensaje["id_mensaje"]))->result_array()[0]["appbuap"];
	?>
	<?php if ($appbuap == "1"): ?>
	<i class="text-right" style="display: block; padding-bottom: 15px;">Enviado desde AppBUAP</i>
	<?php endif; ?>
    <div class="container-fluid container-botones-liberar"> 
        <div class="row">
            <div class="col-lg-12">
                <a title="Asignar" class="btn btn-success col-8 offset-2" id="asignar-<?php echo $mensaje["id_mensaje"]; ?>" onclick="abrirCerrariFrame('<?php echo $mensaje["id_mensaje"]; ?>')" style="color: white; cursor: default;"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Asignar </a>
                
            </div>
                    

        </div>
      </div>
    <div class="container-fluid container-editor">
      <div class="row">
          <iframe src="<?php echo base_url("index.php/admin/asignar_iframe/" . $mensaje["id_mensaje"]); ?>" class="col-12" scrolling=no id="respuesta-<?php echo $mensaje["id_mensaje"]; ?>" style="display: none; height:330px;"></iframe>
      </div>
  </div>
  </div>

</div>
<br>
            <?php endforeach; ?>
        </div>
    </div>
    <?php echo form_close(); ?>
    </div>
    <div id="modalExporta">

    </div>
</body>