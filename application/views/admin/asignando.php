
<style>
     .disabled {
        opacity: 0.9;
        pointer-events: none;
        cursor: default;
       /* background-color: #003b5c !important;*/
    }
</style>
<div class="container-fluid">
    <?php echo form_open(); ?>
    <?php echo validation_errors(); ?>
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <input type="hidden" name="idMensaje" value="<?php echo $idmensaje; ?>" />
            <label for="prioridad">
            Prioridad: &nbsp; &nbsp;
            </label> <br>
            <select name="idprioridad" class="custom-select" id="prioridad" style="width:100%;">
                <?php  foreach ($prioridades as $prioridad): ?>
                    <option value="<?php echo $prioridad["id_prioridad"]; ?>"><?php echo $prioridad["tipo_prioridad"]; ?></option>
                <?php endforeach; ?> 
            </select><br><br>
            <label for="id_dependencia">
            Dependencia a asignar: &nbsp; &nbsp;
            </label> <br>
            <select name="id_dependencia" class="custom-select" id="id_dependencia" style="width:100%;">
                <?php  foreach ($dependencias as $dep): ?>
                    <option value="<?php echo $dep["id_dependencia"]; ?>"><?php echo $dep["dependencia"]; ?></option>
                <?php endforeach; ?> 
            </select>
            <br><br>
            <small>Si considera que el mensaje contiene elementos sensibles, puede seleccionar como anónimo el remitente, con la siguiente opción: <br></small>
            <label class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" name="anonimo">
            <span class="custom-control-indicator"></span>
            <span class="custom-control-description">Asignar como <i>Anónimo</i></span>
          </label><br>
            <button type="submit" class="btn btn-success" id="enviar" style="float: right;">
                <i class="fa fa-arrow-right" aria-hidden="true"></i> Asignar mensaje
            </button>
        </div>

    </div>
</div>
<?php echo form_close(); ?>