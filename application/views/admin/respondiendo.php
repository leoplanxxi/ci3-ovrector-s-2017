<script src="https://cdn.ckeditor.com/4.7.0/basic/ckeditor.js"></script>
<script>
    function deshabilitarBoton() {
        var enviar = document.getElementById("enviar");
        
        enviar.innerHTML = "Enviando correo, espere...";
        enviar.setAttribute("class", "btn btn-success disabled");
    }
</script> 
<style>
     .disabled {
        opacity: 0.9;
        pointer-events: none;
        cursor: default;
       /* background-color: #003b5c !important;*/
    }
</style>
<div class="container-fluid">
    <?php echo form_open(); ?>
    <div class="row">
        <div class="col-12">
            <input type="hidden" name="idsup" value="<?php echo $idsup; ?>" />
            <input type="hidden" name="email" value="<?php echo $email_cliente; ?>" />
            Escriba aquí su respuesta:
            <textarea name="respuesta" class="col-12">
            </textarea>
            <script>
                CKEDITOR.replace( 'respuesta' );
            </script>
        </div>

        <div class="col-lg-3 offset-lg-9 col-md-12" style="padding-top: 0.5rem">
            <button type="submit" class="btn btn-success" id="enviar" style="width: 100%" onclick="javascript:deshabilitarBoton()">
                <i class="fa fa-paper-plane" aria-hidden="true"></i> Enviar Respuesta
            </button>
        </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>