<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.6/footable.min.js" integrity="sha256-R3NffQkOJUqmiutQHnxEURXUXZru/7GMdM6CdH673Qw=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.6/footable.standalone.min.css" integrity="sha256-gMgjLCY647lV6R/l2EPo/jAWuDPQSyIirjBmeX+8p1o=" crossorigin="anonymous" />

<script>
jQuery(function($){
	$('#historial-libera').footable();
});
</script>

<body class="page-historial-asigna">
<div class="container">
<div class="row">
<div class="col-12"><br>
<h2 class="text-center">Mensajes liberados</h2>

<a href="<?php echo base_url("index.php/admin/supervision"); ?>" class="btn btn-outline-primary"><i class="fa fa-arrow-left"></i> Regresar</a>
	<table class="table" id="historial-libera" style="table-layout: fixed;">
	<thead>
		<tr>
			
			<th>Usuario</th>
			<th>Mensaje</th>
			<th>Respuesta</th>
			<th>Desliberar</th>
			<th data-breakpoints="all">Dependencia</th>
			<th data-breakpoints="all">Usuario que asignó</th>
			<th data-breakpoints="all">Fecha de asignación</th>
			<th data-breakpoints="all">Usuario que responde</th>
			<th data-breakpoints="all">Fecha de respuesta</th>
			<th data-breakpoints="all">Liberador por</th>
			<th data-breakpoints="all">Fecha de liberación</th>			<?php // ¿Es necesario ver que el supervisor lo modifico? ?>
			
		</tr>
	</thead>
	<tbody>
		<?php foreach ($mensajes as $mensaje): ?>
		<tr>
			
			<td><?php 
				echo $mensaje->nombre_cliente;
			?></td>
			<td><?php echo $mensaje->mensaje; ?></td>
			<td><?php 
				echo $mensaje->respuesta_mensaje;
			?></td>
			<td><a href="<?php echo base_url("index.php/admin/desliberar/" . $mensaje->id_seguimiento); ?>"><i class="fa fa-unlock-alt" aria-hidden="true"></i>  Desliberar</a></td>
			<td><?php echo $mensaje->dependencia_destino; ?></td>
			<td><?php $usuario_asigna = $this->db->query("SELECT nombre_usuario FROM tb_usuarios WHERE id_usuario = ?", array($mensaje->Usuario_Asigna_ID))->result_array()[0]["nombre_usuario"];
				echo $usuario_asigna; ?></td>
			<td><?php echo $mensaje->fecha_asignacion; ?></td>
			<td><?php $usuario_responde = $this->db->query("SELECT nombre_usuario FROM tb_usuarios WHERE id_usuario = ?", array($mensaje->Usuario_Responde_ID))->result_array()[0]["nombre_usuario"];
				echo $usuario_responde; ?></td>
			<td><?php echo $mensaje->fecha_respuesta; ?></td>
			<td><?php $usuario_libera = $this->db->query("SELECT nombre_usuario FROM tb_usuarios WHERE id_usuario = ?", array($mensaje->Usuario_libera_ID))->result_array()[0]["nombre_usuario"];
				echo $usuario_libera; ?></td>
			<td><?php echo $mensaje->fecha_liberado; ?></td>
			
		</tr>
		<?php endforeach; ?>
	</tbody>
	</table>
	<?php echo $links; ?>
</div>
</div>
</div>
</body>