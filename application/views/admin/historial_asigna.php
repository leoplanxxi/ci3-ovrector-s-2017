<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.6/footable.min.js" integrity="sha256-R3NffQkOJUqmiutQHnxEURXUXZru/7GMdM6CdH673Qw=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.6/footable.standalone.min.css" integrity="sha256-gMgjLCY647lV6R/l2EPo/jAWuDPQSyIirjBmeX+8p1o=" crossorigin="anonymous" />

<script>
jQuery(function($){
	$('#historial-libera').footable();
});
</script>

<body class="page-historial-asigna">
<div class="container">
<div class="row">
<div class="col-12"><br>
<h2 class="text-center">Mensajes asignados</h2>

<a href="<?php echo base_url("index.php/admin/moderacion"); ?>" class="btn btn-outline-primary"><i class="fa fa-arrow-left"></i> Regresar</a>
	<table class="table" id="historial-libera" style="table-layout: fixed;">
	<thead>
		<tr>
			<th>Usuario</th>
			<th>Mensaje</th>
			<th data-breakpoints="xs sm">Fecha de recepción</th>
			<th data-breakpoints="xs sm">Dependencia</th>
			<th data-breakpoints="xs sm">Usuario que asignó</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($mensajes as $mensaje): ?>
		<tr>
			<td><?php 
				$nombre_cliente = $this->db->query("SELECT nombre_cliente FROM mensaje WHERE id_mensaje = ?", array($mensaje->id_mensaje))->result_array()[0]["nombre_cliente"];
				echo $nombre_cliente;
			?></td>
			<td><?php echo $mensaje->mensaje; ?></td>
			<td><?php 
				$fecha_recepcion = $this->db->query("SELECT fecha FROM mensaje WHERE id_mensaje = ?", array($mensaje->id_mensaje))->result_array()[0]["fecha"];
				
				echo gmdate("d-m-Y", $fecha_recepcion);
				//echo gmdate("Y-m-d\TH:i:s\Z", $fecha_recepcion);
			?></td>
			<td><?php echo $mensaje->dependencia_destino; ?></td>
			<td><?php $usuario_asigna = $this->db->query("SELECT nombre_usuario FROM tb_usuarios WHERE id_usuario = ?", array($mensaje->Usuario_Asigna_ID))->result_array()[0]["nombre_usuario"];
				echo $usuario_asigna; ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
	</table>
	<?php echo $links; ?>
</div>
</div>
</div>
</body>