
<div class="modal fade" id="modalExportar" tabindex="-1" role="dialog">
<script>
    $(document).ready(function() {
        document.getElementById('inicial').valueAsDate = new Date();
        document.getElementById('final').valueAsDate = new Date();
    });
</script>
<style>
    .group {
        border: 1px solid #cecece;
        padding: 20px;
    }
</style>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Exportar a CSV</h4>  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
            </div>
            <?php echo form_open(base_url("index.php/admin/exportarCSV_fecha")); ?>

            <div class="modal-body">

            <p>Seleccione el rango de fechas de los mensajes que desea exportar: </p>
            <label class="custom-control custom-radio">
                <input type="radio" name="exportar" value="sinliberar"  class="custom-control-input" checked>  <span class="custom-control-indicator"></span>
                <span class="custom-control-description">Exportar todos los mensajes sin liberar</span><br>
            </label>
            <label class="custom-control custom-radio">
                <input type="radio" name="exportar" value="todo"  class="custom-control-input" checked>  <span class="custom-control-indicator"></span>
                <span class="custom-control-description">Exportar todos los mensajes liberados</span><br>
            </label>
            <label class="custom-control custom-radio">
                <input type="radio" name="exportar" value="fecha"  class="custom-control-input"> 
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">Exportar los mensajes liberados por rango de fechas</span>
            </label>    
            <br><br>
            <div class="group">
                <p>Fecha de inicio: <br>
                <input type="date" name="inicial" class="form-control" id="inicial"></p>
                <p>Fecha de fin: <br>
                <input type="date" name="final" class="form-control" id="final"></p>
            </div>
            </div>
            <div class="modal-footer">
                <input type="submit" value="Exportar" class="btn btn-primary" style="float: right;">
                <input type="submit" value="Cerrar" class="btn btn-danger" style="float: left;" data-dismiss="modal">
            </div>
            <?php echo form_close(); ?>

        </div>
    </div>
</div> 