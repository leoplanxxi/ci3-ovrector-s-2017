<style>
	.nav-item-logout {
		float: left;
		padding-left: 19px;
		margin-top: -8px;
	}
</style>

<nav class="navbar navbar-toggleable-md navbar-buap navbar-inverse bg-faded">
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="#">&nbsp;</a>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url("index.php/admin/moderacion"); ?>"><i class="fa  fa-user-secret" aria-hidden="true"></i> Módulo de Moderador</a> 
      </li> - 
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url("index.php/admin/supervision"); ?>"><i class="fa fa-user" aria-hidden="true"></i> Módulo de Supervisor</a>
      </li>

     
      <div class="float-right nav-usuario">
          <span style="float: left;">Bienvenido, <?php
            echo $this->session->userdata("logged_in")["nombre"]; //Nombre de usuario
          ?></span>
		   <li class="nav-item active nav-item-logout">
        <a class="nav-link" href="<?php echo base_url("index.php/login/logout"); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i> Cerrar sesión</a>
      </li>
      </div>
    </ul>
   
  </div>
</nav>