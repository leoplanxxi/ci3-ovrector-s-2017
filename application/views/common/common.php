<meta name="viewport" content="width=device-width, initial-scale=1">

<script src="<?php echo base_url("public/scripts.js"); ?>"></script>
<script src="<?php echo base_url("public/lib/jquery-3.2.1.min.js"); ?>"></script>
<script src="<?php echo base_url("public/lib/hammer.min.js"); ?>"></script>
<script src="<?php echo base_url("public/lib/bs4/js/bootstrap.min.js"); ?>"></script>
<!--<script src="<?php echo base_url("public/lib/footable/js/footable.min.js"); ?>"></script>-->
<script src="//cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>

<link rel="stylesheet" href="<?php echo base_url("public/styles.css") ?>" />
<link rel="stylesheet" href="<?php echo base_url("public/lib/bs4/css/bootstrap.min.css") ?>" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
<!--<link rel="stylesheet" href="<?php echo base_url("public/lib/footable/css/footable.standalone.min.css") ?>" />-->
