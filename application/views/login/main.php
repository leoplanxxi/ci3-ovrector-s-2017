<body class="login-page">
	<style>
	.header {
		padding-top: 15px;
		padding-bottom: 15px;
		background: #00395E;
		margin-bottom: 40px;
		color: white;
		font-weight: bold;
		padding-left: 20%;
		font-size: 18px;
	}
	.row-top-login-lns {
		height: 150px;
		background: url("<?php echo base_url("public/Carolino.png"); ?>");
		background-size: 100%;
		background-position: center;
	}

	.row-top-login-buap img {
		width: 90px;
		height: 90px;
		border: 3px solid #fff;
		display: block;
		margin: 0 auto;
		margin-top: -45px;
	}

	@media only screen and (max-width: 992px) {
		.header img {
			display: none;
		}

		.header {
			padding-left: 10px;
			text-overflow: ellipsis;
			overflow: hidden;
			white-space: nowrap;
			margin-bottom: 5px;

		}
	}
	</style>
<script type="text/javascript">
function image(){
 $.post('<?php echo base_url('index.php/login/refresh_captcha');?>',
   {'<?php echo $this->security->get_csrf_token_name();?>':'<?php echo $this->security->get_csrf_hash(); ?>'},function(resp){
    $('.imag').html(resp);
  });
  }
</script>
	<div class="container-fluid">
		<div class="header">
			<a href="http://www.buap.mx"><img src="<?php echo base_url("public/download.png"); ?>" style="height: 30px"></a>
Seguimiento de la Oficina Virtual del Rector
				</div>
			</div>
			<div class="container">

				<div class="row">
					<div class="col-lg-6 offset-lg-3 col-container-login">
						<?php
  echo form_open();
?>

						<div class="row row-top-login-lns">
						</div>
						<div class="row row-top-login-buap">
							<img src="<?php echo base_url("public/Escudo_0.png"); ?>" class="rounded-circle">
								</div>

								<div class="form-group row">
									<?php if ($this->session->flashdata("error")): ?>
									<div class="col-lg-10 offset-lg-1">
										<div class="alert alert-danger" role="alert">
											<?php echo $this->session->flashdata("error"); ?>
										</div>

									</div>
									<?php endif; ?>
									<div class="col-lg-10 offset-lg-1">
										<label for="example-text-input" class="col-form-label">Usuario:</label> 

										<input class="form-control" type="text" name="usuario" id="usuario">
										</div>
									</div>

									<div class="form-group row">
										<div class="col-lg-10 offset-lg-1">
											<label for="example-text-input" class="col-form-label">Contraseña:</label>

											<input class="form-control" type="password" name="password" id="password">
											</div>
										</div>
										<!-- Captcha-->
										<div class="form-group">
                			<div class="col-lg-10 offset-lg-1">
                 				<span class="imag"><?=$captcha['image']?></span>
                 				<span><a class="refresh" onclick="image()" href="javascript:;"><img src="<?php echo base_url('public/refresh.png')?>"/></a></span>
                			</div>
            				</div>

              			<div class="form-group">
                			<div class="col-lg-10 offset-lg-1">
                  				<input type="text" id="captcha" name="captcha" class="form-control" placeholder="captcha" required/>
                  				<input type="hidden" value="<?=$captcha['word']?>" name="string_captcha" />
                			</div>
              		</div>
										<br>
											<div class="form-group row">
												<div class="col-lg-10 offset-lg-1 text-center">
													<input class="btn btn-info" type="submit" name="entrar" id="entrar" value="Entrar">
													</div>
												</div>
												<?php
												  echo form_close();
												?>
											</div>
										</div>
									</div>

								</body>
