<?php
    Class Admin extends CI_Controller {
        public function __construct() {
            parent::__construct();
            if (!$this->session->userdata("logged_in")) {
                redirect("login");
            }
        }
        public function index() {
            redirect("admin/moderacion"); 
            // Verificar esto

        }
        public function moderacion() {

            $this->load->model("supervision_model");
            $this->load->model("moderacion_model");
            $this->load->helper("ovrector_helper");

            $data["mensajes"] = $this->moderacion_model->getAllModeracion();

            $this->load->view("common/common");
            $this->load->view("admin/moderador", $data);
        }

        public function supervision() {
            $this->load->model("supervision_model");
            $this->load->helper("ovrector_helper");

            $data["mensajes"] = $this->supervision_model->getAllMensajes();

            $this->load->view("common/common");
            $this->load->view("admin/supervisor", $data);
        }

        public function liberarsMensaje($idSupervision) {
            $this->load->library("form_validation");
            $this->load->model("supervision_model");
            $this->load->model("seguimiento_model");

            $this->form_validation->set_rules("idseg", "ID seguimiento", "required|integer");

            if ($this->form_validation->run() === FALSE) {
                $data["idseg"] = $idSupervision;
                $this->load->view("common/common");
                $this->load->view("admin/confirma_liberasm", $data);
            }
            else {
				$idUsuario_libera = $this->session->userdata("logged_in")["idusuario"];
                $idSeguimiento = $this->secclean->limpiar($this->input->post("idseg"));
                $this->seguimiento_model->liberarMensajesEmail($idUsuario_libera, $idSeguimiento);
                redirect("admin/supervision");
            }
        }
		/*
		public function historial_asignados() {
			$this->load->model("historial_model");
			
			$this->load->view("common/common");
			$this->load->view("common/menu");
			
			$data["mensajes"] = $this->historial_model->historialAsigna();
			
			$this->load->view("admin/historial_asigna", $data);
		} */
		public function historial_asignados() {
			$this->load->model("historial_model");
			$this->load->model("supervision_model");
			$this->load->library("pagination");
			$this->load->helper("url");
			
			$config = array();
			$config["base_url"] = base_url() . "index.php/admin/historial_asignados";
			$config["total_rows"] = count($this->historial_model->historialAsigna());
			$config["per_page"] = 10;
			$config["uri_segment"] = 3;
			
			$this->pagination->initialize($config);
			
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$data["mensajes"] = $this->historial_model->getHistorialAsigna_pag($config["per_page"], $page);
			$data["links"] = $this->pagination->create_links();

			$this->load->view("common/common");
			$this->load->view("common/menu");
					
			$this->load->view("admin/historial_asigna", $data);
		}
		
		public function historial_liberados() {
			$this->load->model("historial_model");
			$this->load->model("supervision_model");
			$this->load->library("pagination");
			$this->load->helper("url");
			
			$config = array();
			$config["base_url"] = base_url() . "index.php/admin/historial_liberados";
			$config["total_rows"] = count($this->historial_model->historialLibera());
			$config["per_page"] = 10;
			$config["uri_segment"] = 3;
			
			$this->pagination->initialize($config);
			
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$data["mensajes"] = $this->supervision_model->getHistorialLibera_pag($config["per_page"], $page);
			$data["links"] = $this->pagination->create_links();

			$this->load->view("common/common");
			$this->load->view("common/menu");
					
			$this->load->view("admin/historial_libera", $data);
		}
		/*public function historial_liberados() {
			$this->load->model("historial_model");
			
			$this->load->view("common/common");
			$this->load->view("common/menu");
			
			$data["mensajes"] = $this->historial_model->historialLibera();
			
			$this->load->view("admin/historial_libera", $data);
		}*/

        public function asignar_iframe($idmensaje) {
            $this->load->library("form_validation");
            $this->load->model("supervision_model");
            $this->load->model("dependencias_model");
            $this->load->model("seguimiento_model");
            $this->load->model("moderacion_model");

            $this->form_validation->set_rules("idprioridad", "Prioridad", "required");
            $this->form_validation->set_rules("id_dependencia", "Dependencia", "required");
            $this->form_validation->set_rules("idMensaje", "ID Mensaje", "required");

            if ($this->form_validation->run() === FALSE) {
                $data["idmensaje"] = $idmensaje;
                $data["prioridades"] = $this->moderacion_model->getAllPrioridades();
                $data["dependencias"] = $this->dependencias_model->getAllDependencias();

                $this->load->view("common/common");
                $this->load->view("admin/asignando", $data);

            }
            else {
                $idMensaje = $this->secclean->limpiar($this->input->post("idMensaje"));
                $idPrioridad = $this->secclean->limpiar($this->input->post("idprioridad"));
                $idDependencia = $this->secclean->limpiar($this->input->post("id_dependencia"));

                if ($this->input->post("anonimo") != NULL) {
                    $anonimo = 1;
                }
                else {
                    $anonimo = 0;
                }
                $this->seguimiento_model->asignarMensaje($idMensaje, $idPrioridad, $idDependencia, $anonimo);
                $this->db->query("UPDATE contacto.mensaje SET estado_mensaje = 1 WHERE  mensaje.id_mensaje = ?", $idMensaje); 
                echo "<script>";
                echo "window.top.location.href = '" . base_url("index.php/admin/moderacion") . "';</script>";
               
            }
        }

        public function responder_iframe($idSupervision) {
            $this->load->library("form_validation");
            $this->load->model("supervision_model");
            $this->load->model("seguimiento_model");

            $this->form_validation->set_rules("respuesta", "Respuesta", "required");
            $this->form_validation->set_rules("email", "Email", "required|valid_email");
            $this->form_validation->set_rules("idsup", "ID Supervisión", "required|integer");
            if ($this->form_validation->run() === FALSE) {
                $data["idsup"] = $idSupervision;
                $data["email_cliente"] = $this->supervision_model->getMensajeEmailCliente($idSupervision);
                $this->load->view("common/common");
            
                $this->load->view("admin/respondiendo", $data);
            }
            else {
                $email = $this->secclean->limpiar($this->input->post("email"));
                $mensj = $this->secclean->limpiarwoHTML($this->input->post("respuesta"));
                $idSup = $this->secclean->limpiar($this->input->post("idsup"));

                $this->helper->sendEmail($mensj, $email);
				$idUsuario_libera = $this->session->userdata("logged_in")["idusuario"];

                $this->seguimiento_model->liberarMensajecEmail($idUsuario_libera, $idSup, $mensj);

                echo "<script>";
                echo "window.top.location.href = '" . base_url("index.php/admin/supervision") . "';</script>";

            }
            
        }
		
		public function historial_libera() {
			$this->load->model("supervision_model");
            $this->load->helper("ovrector_helper");
			$this->load->library('pagination');

            $data["mensajes"] = $this->supervision_model->getHistorialLibera();
			
			$config['base_url'] =  base_url("index.php/admin/historial_libera");
			$config['per_page'] = 20;

			$this->pagination->initialize($config);

            $this->load->view("common/common");
            $this->load->view("admin/historial_libera", $data);
			echo $this->pagination->create_links();
		}
		
		public function desliberar($idSeguimiento) {
			$this->load->model("seguimiento_model");
			$this->seguimiento_model->desliberarMensaje($idSeguimiento);
			redirect("admin/supervision");
		}
		
		public function archivar($idSeguimiento) {
			$this->load->model("seguimiento_model");
			$this->seguimiento_model->purgarMensaje($idSeguimiento);
			redirect("supervision");
		}
		
		public function desarchivar($idSeguimiento) {
			$this->load->model("seguimiento_model");
			$this->seguimiento_model->despurgarMensaje($idSeguimiento);
			redirect("supervision");
		}
		
		public function exportarCSV($actividad) {
            $this->load->model("exportar_model");
			if ($actividad == 1) {
				$query = $this->exportar_model->getAllExportar();
				$filename = "liberados.csv";
			}
			else if ($actividad == 2) {
				$query = $this->exportar_model->getAllExportarSLiberar();
				$filename = "sin_liberar.csv";
            }
			else {
				return false;
			}
			$this->load->dbutil();
			$this->load->helper('file');
			$this->load->helper('download');
			$delimiter = ",";
			$newline = "\r\n";
			$result = $query;
			$data = $this->dbutil->csv_from_result($result, $delimiter, $newline);
            force_download($filename, $data);
            redirect("admin/supervision", refresh);
        }
        
        public function exportarCSV_fecha() {
            $this->load->library("form_validation");
            $this->load->model("exportar_model");

            $this->form_validation->set_rules("inicial", "Fecha inicial", "required");
            $this->form_validation->set_rules("final", "Fecha final", "required");

            if ($this->form_validation->run() === FALSE) {
                //$this->load->view("common/common");
                $this->load->view("admin/exportar_csv_fecha");
            }
            else {
                $exportar_valor = $this->secclean->limpiar($this->input->post("exportar"));
                if ($exportar_valor == "fecha") {
                    $query = $this->exportar_model->getAllExportarFechas();
                    $filename = "liberados.csv";
                    $this->load->dbutil();
                    $this->load->helper('file');
                    $this->load->helper('download');
                    $delimiter = ",";
                    $newline = "\r\n";
                    $result = $query;
                    $data = $this->dbutil->csv_from_result($result, $delimiter, $newline);
                    force_download($filename, $data);
                    redirect("admin/supervision", refresh);
                }
                else {
                    redirect("admin/exportarCSV/1");
                }
            }
        }
    }