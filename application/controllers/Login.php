<?php
  Class Login extends CI_Controller {
    public function __construct() {
      parent::__construct();
      $this->load->helper('string');
      $this->load->helper('captcha');
      $this->rand = random_string('alnum',5);
     $this->load->model('captcha_model');
    }

    public function captcha()
       {
      //configuramos el captcha
      $conf_captcha = array(
        'word'   => $this->rand,
        'img_path' => './public/captcha/',
        'img_url' =>  base_url().'public/captcha/',
        'font_path' => './public/tipografias/comic.ttf',
        'img_width' => '200',
        'img_height' => '60',
        'font_size' => 28,
        //decimos que pasados 10 minutos elimine todas las imágenes
        //que sobrepasen ese tiempo
        'expiration' => 600,
           'colors'        => array(
                  'background' => array(255, 255, 255),
                  'border' => array(255, 255, 255),
                  'text' => array(0, 0, 0),
                  'grid' => array(255, 40, 40)
          )
      );

      //guardamos la info del captcha en $cap
      $cap = create_captcha($conf_captcha);

      //pasamos la info del captcha al modelo para
      //insertarlo en la base de datos
      $this->captcha_model->nuevo_captcha($cap,$this->helper->getRealIP());
        $this->session->set_userdata('captcha', $this->rand);
      //devolvemos el captcha para utilizarlo en la vista
      return $cap;
    }
    public function refresh_captcha(){

      //configuramos el captcha
        $this->rand = random_string('alnum',5);
        $this->session->set_userdata('captcha', $this->rand);
      $conf_captcha = array(
        'word'   => $this->rand,
        'img_path' => './public/captcha/',
        'img_url' =>  base_url().'public/captcha/',
        'font_path' => './public/tipografias/comic.ttf',
        'img_width' => '175',
        'img_height' => '60',
        'font_size' => 28,
        //decimos que pasados 10 minutos elimine todas las imágenes
        //que sobrepasen ese tiempo
        'expiration' => 600,
           'colors'        => array(
                  'background' => array(255, 255, 255),
                  'border' => array(255, 255, 255),
                  'text' => array(0, 0, 0),
                  'grid' => array(255, 40, 40)
          )
      );

      //guardamos la info del captcha en $cap
      $cap = create_captcha($conf_captcha);

      //pasamos la info del captcha al modelo para
      //insertarlo en la base de datos
      $this->captcha_model->nuevo_captcha($cap,$this->helper->getRealIP());

      //devolvemos el captcha para utilizarlo en la vista
      echo $cap['image'];
    }
    //comprobamos si la sessión que hemos creado es igual que el captcha introducido
    	//con una función callback
    public function validate_captcha()
    	{

    	    if($this->input->post('captcha') != $this->session->userdata('captcha'))
    	    {
    	         $this->session->set_flashdata("error", "Captcha Incorrecto");
    	        return false;
    	    }else{
    	        return true;
    	    }

    	}
    public function index() {
      if ($this->session->userdata("logged_in")) {
        redirect("admin");
      }
      $this->load->model("usuarios_model");

      $this->form_validation->set_rules("usuario", "Nombre de usuario", "required");
      $this->form_validation->set_rules("password", "Contraseña", "required");
      $this->form_validation->set_rules('captcha', 'Captcha', 'callback_validate_captcha|required|trim|xss_clean');

      if ($this->form_validation->run() === FALSE) {
      
        $data['captcha'] =$this->captcha();
        $this->load->view("common/common");
        $this->load->view("login/main",$data);
      }
      else {
        //checar captcha

        $expiration = time()-600; // Límite de 10 minutos
                    $ip = $this->helper->getRealIP();//ip del usuario
                    $captcha = $this->input->post('captcha');//captcha introducido por el usuario

                     //eliminamos los captcha con más de 10 minutos de vida
                    $this->captcha_model->eliminar_old_captcha($expiration);


                    //comprobamos si es correcta la imagen introducida
                    $check = $this->captcha_model->buscar($ip,$expiration,$captcha);
                    if($check == 1)
                     {
                     $nombre = $this->secclean->limpiar($this->input->post("usuario"));
                     $passwd = $this->secclean->limpiar($this->input->post("password"));

                     $login = $this->usuarios_model->checkLogin($nombre, $passwd);
                     if ($login == TRUE) {
                       $idusuario = $this->usuarios_model->getUsuariobyName($nombre)["id_usuario"];
                       $datos_usuario = $this->usuarios_model->getUsuario($idusuario);
                       $sess_data = array(
                         'usuario' => $datos_usuario["usuario"],
                         'nombre' => $datos_usuario["nombre_usuario"],
                         'idrol' => $datos_usuario["id_rol"],
                         'iddependencia' => $datos_usuario["id_dependencia"],
                         'idusuario' => $datos_usuario["id_usuario"]
                       );

                       $this->session->set_userdata("logged_in", $sess_data);
                       redirect("admin/");
                     }
                     else {
                       $this->session->set_flashdata("error", "Su nombre de usuario o contraseña es incorrecto. Favor de verificarlo");
                       redirect("login");
                     }
                   }//captcha


      }//else

    }

    public function logout() {
      $this->session->sess_destroy();
      redirect("login");
    }
  }

 ?>
