<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Helper {

    public function __construct() {
        $this->CI =& get_instance();
        $this->CI->load->library("email");
        $this->CI->load->database();
    }

    public function sendEmailNative($msg, $to) {
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'utf-8';
        $config['mailtype'] = 'html';
        $config['wordwrap'] = TRUE;

        $this->CI->email->initialize($config);
        $this->CI->email->set_crlf( "\r\n" );

        $message = $msg;
        $message = $message . "<br /><hr />Este correo no está habilitado para recibir respuestas. Agradecemos tus comentarios y sugerencias en el siguiente <a href='http://www.alfonsoesparza.buap.mx/'>vínculo</a>.";

        $this->CI->email->clear();
        $this->CI->email->from("ovrector@correo.buap.mx", "OV Rector");
        $this->CI->email->to($to);
        $this->CI->email->subject("Respuesta a su mensaje");
        $this->CI->email->message($message);

        if($this->CI->email->send()===TRUE){
            echo "Enviado exitosamente a " . $to . "<br />";
            return true;
        }
        else {
            echo "Error al enviar a " . $to . "<br />";
                $this->CI->email->print_debugger();

            return false;
        }
    }

    public function sendEmail($msg, $to) {
        $sendEmail = "seguimiento.rector@buap.mx";
        $passEmail = "Buap12345";

        $config['protocol']    = 'smtp';
        $config['smtp_host']    = 'smtp.office365.com';
        $config['smtp_port']    = '587';
        $config['smtp_crypto'] = 'tls';
        $config['smtp_timeout'] = '5';
        $config['newline'] = "\r\n";
        $config['smtp_user']    = $sendEmail;
        $config['smtp_pass']    = $passEmail;
        $config['charset']    = 'utf-8';

        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not

        $this->CI->email->initialize($config);
        $this->CI->email->set_crlf( "\r\n" );

        $message = $msg;
        $message = $message . "<br /><hr />Este correo no está habilitado para recibir respuestas. Agradecemos tus comentarios y sugerencias en el siguiente <a href='http://www.alfonsoesparza.buap.mx/'>vínculo</a>.";

        $this->CI->email->clear();
        $this->CI->email->from($sendEmail);
        $this->CI->email->to($to);
        $this->CI->email->subject("Respuesta a su mensaje");
        $this->CI->email->message($message);

        if($this->CI->email->send()===TRUE){
            echo "Enviado exitosamente a " . $to . "<br />";
            return true;
        }
        else {
            echo "Error al enviar a " . $to . "<br />";
                $this->CI->email->print_debugger();

            return false;
        }
    }
    public function getRealIP()
{

   if (isset($_SERVER["HTTP_CLIENT_IP"]))
   {
       return $_SERVER["HTTP_CLIENT_IP"];
   }
   elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
   {
       return $_SERVER["HTTP_X_FORWARDED_FOR"];
   }
   elseif (isset($_SERVER["HTTP_X_FORWARDED"]))
   {
       return $_SERVER["HTTP_X_FORWARDED"];
   }
   elseif (isset($_SERVER["HTTP_FORWARDED_FOR"]))
   {
       return $_SERVER["HTTP_FORWARDED_FOR"];
   }
   elseif (isset($_SERVER["HTTP_FORWARDED"]))
   {
       return $_SERVER["HTTP_FORWARDED"];
   }
   else
   {
       return $_SERVER["REMOTE_ADDR"];
   }

}

}
?>
